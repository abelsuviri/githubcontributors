package com.abelsuviri.data.repository

import com.abelsuviri.data.model.ContributorsModel
import com.abelsuviri.data.model.UserModel
import com.abelsuviri.data.network.GitHubService
import com.abelsuviri.data.util.Result
import java.lang.Exception

/**
 * @author Abel Suviri
 */

class GitHubRepository constructor(private val gitHubService: GitHubService) {

    /**
     * Call to retrieve the contributors
     */
    suspend fun retrieveContributors(): Result<List<ContributorsModel>> {
        return try {
            val response = gitHubService.retrieveContributors()
            val result = response.await()

            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    /**
     * Call to get more info about an user. Necessary to get user location
     */
    suspend fun getUserInfo(username: String): Result<UserModel> {
        return try {
            val response = gitHubService.getUserInfo(username)
            val result = response.await()

            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}