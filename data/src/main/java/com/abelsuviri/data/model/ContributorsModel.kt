package com.abelsuviri.data.model

import com.google.gson.annotations.SerializedName

/**
 * @author Abel Suviri
 */

data class ContributorsModel(
    @SerializedName("login") val userName: String,
    @SerializedName("avatar_url") val avatar: String,
    @SerializedName("contributions") val commits: Int
)