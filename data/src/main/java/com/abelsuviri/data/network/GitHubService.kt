package com.abelsuviri.data.network

import com.abelsuviri.data.model.ContributorsModel
import com.abelsuviri.data.model.UserModel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author Abel Suviri
 */

interface GitHubService {
    //Retrieve repository contributors
    @GET("repos/android/android-ktx/contributors?q=contributions&order=desc&per_page=15")
    fun retrieveContributors(): Deferred<List<ContributorsModel>>

    //Get more info about an user
    @GET("/users/{username}")
    fun getUserInfo(@Path("username") username: String): Deferred<UserModel>

    fun getService(): GitHubService {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit.create(GitHubService::class.java)
    }
}

private const val baseUrl = "https://api.github.com/"