package com.abelsuviri.viewmodel

import androidx.lifecycle.MutableLiveData
import com.abelsuviri.data.model.ContributorsModel
import com.abelsuviri.data.repository.GitHubRepository
import com.abelsuviri.data.util.Result
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * @author Abel Suviri
 */

class ContributorsViewModel constructor(private val gitHubRepository: GitHubRepository) : BaseViewModel() {

    val contributorsList: MutableLiveData<List<ContributorsModel>> = MutableLiveData()

    /**
     * Executes the call to retrieve the list of contributors
     */

    fun getContributors() {
        isLoading.value = true
        GlobalScope.launch {
            val response = gitHubRepository.retrieveContributors()
            if (response is Result.Success) {
                contributorsList.postValue(response.data)
                setSuccess()
            } else {
                setFailure()
            }
        }
    }
}