package com.abelsuviri.viewmodel

import androidx.lifecycle.MutableLiveData
import com.abelsuviri.data.repository.GitHubRepository
import com.abelsuviri.data.util.Result
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * @author Abel Suviri
 */

class UserLocationViewModel constructor(private val gitHubRepository: GitHubRepository) : BaseViewModel() {

    val userLocation: MutableLiveData<String> = MutableLiveData()

    /**
     * Executes the call to retrieve the user location
     */

    fun getContributorLocation(username: String) {
        isLoading.value = true
        GlobalScope.launch {
            val response = gitHubRepository.getUserInfo(username)
            if (response is Result.Success) {
                userLocation.postValue(response.data.location)
                setSuccess()
            } else {
                setFailure()
            }
        }
    }
}