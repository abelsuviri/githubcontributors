package com.abelsuviri.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * @author Abel Suviri
 */

open class BaseViewModel : ViewModel() {
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val hasFailed: MutableLiveData<Boolean> = MutableLiveData()

    fun setSuccess() {
        isLoading.postValue(false)
        hasFailed.postValue(false)
    }

    fun setFailure() {
        isLoading.postValue(false)
        hasFailed.postValue(true)
    }
}