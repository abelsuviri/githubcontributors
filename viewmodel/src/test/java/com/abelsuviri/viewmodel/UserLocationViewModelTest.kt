package com.abelsuviri.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import com.abelsuviri.data.model.UserModel
import com.abelsuviri.data.repository.GitHubRepository
import com.abelsuviri.data.util.Result
import com.abelsuviri.viewmodel.mock.MockJson
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * @author Abel Suviri
 */

class UserLocationViewModelTest {
    @Mock
    lateinit var repository: GitHubRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var lifecycleOwner: LifecycleOwner

    lateinit var lifecycleRegistry: LifecycleRegistry

    private lateinit var gson: Gson
    private lateinit var userLocationViewModel: UserLocationViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        userLocationViewModel = UserLocationViewModel(repository)
        lifecycleRegistry = LifecycleRegistry(lifecycleOwner)
        Mockito.`when`(lifecycleOwner.lifecycle).thenReturn(lifecycleRegistry)
        gson = Gson()
    }

    @Test
    fun test_get_contributors_list_successfully() {
        val responseModel: Result<UserModel> = Result.Success(gson.fromJson(MockJson.mockUser, UserModel::class.java))

        Mockito.`when`(runBlocking { repository.getUserInfo("") }).thenReturn(responseModel)

        userLocationViewModel.getContributorLocation("")

        userLocationViewModel.userLocation.observe(lifecycleOwner, Observer {
            Assert.assertEquals(userLocationViewModel.userLocation.value, responseModel)
            Assert.assertEquals(userLocationViewModel.isLoading, false)
            Assert.assertEquals(userLocationViewModel.hasFailed, false)
        })
    }

    @Test
    fun test_get_contributors_list_unsuccessfully() {
        val throwable = Result.Error(Throwable())

        Mockito.`when`(runBlocking { repository.retrieveContributors() }).thenReturn(throwable)

        userLocationViewModel.getContributorLocation("")

        userLocationViewModel.userLocation.observe(lifecycleOwner, Observer {
            Assert.assertEquals(userLocationViewModel.userLocation.value, null)
            Assert.assertEquals(userLocationViewModel.isLoading, false)
            Assert.assertEquals(userLocationViewModel.hasFailed, true)
        })
    }
}