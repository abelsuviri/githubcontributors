package com.abelsuviri.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.Observer
import com.abelsuviri.data.model.ContributorsModel
import com.abelsuviri.data.repository.GitHubRepository
import com.abelsuviri.data.util.Result
import com.abelsuviri.viewmodel.mock.MockJson
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * @author Abel Suviri
 */

class ContributorsViewModelTest {

    @Mock
    lateinit var repository: GitHubRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var lifecycleOwner: LifecycleOwner

    lateinit var lifecycleRegistry: LifecycleRegistry

    private lateinit var gson: Gson
    private lateinit var contributorsViewModel: ContributorsViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        contributorsViewModel = ContributorsViewModel(repository)
        lifecycleRegistry = LifecycleRegistry(lifecycleOwner)
        `when`(lifecycleOwner.lifecycle).thenReturn(lifecycleRegistry)
        gson = Gson()
    }

    @Test
    fun test_get_contributors_list_successfully() {
        val typeToken = object : TypeToken<List<ContributorsModel>>(){}.type
        val responseModel: Result<List<ContributorsModel>> = Result.Success(gson.fromJson(MockJson.mockRepositoryContributors, typeToken))

        Mockito.`when`(runBlocking { repository.retrieveContributors() }).thenReturn(responseModel)

        contributorsViewModel.getContributors()

        contributorsViewModel.contributorsList.observe(lifecycleOwner, Observer {
            Assert.assertEquals(contributorsViewModel.contributorsList.value, responseModel)
            Assert.assertEquals(contributorsViewModel.isLoading, false)
            Assert.assertEquals(contributorsViewModel.hasFailed, false)
        })
    }

    @Test
    fun test_get_contributors_list_unsuccessfully() {
        val throwable = Result.Error(Throwable())

        Mockito.`when`(runBlocking { repository.retrieveContributors() }).thenReturn(throwable)

        contributorsViewModel.getContributors()

        contributorsViewModel.contributorsList.observe(lifecycleOwner, Observer {
            Assert.assertEquals(contributorsViewModel.contributorsList.value, null)
            Assert.assertEquals(contributorsViewModel.isLoading, false)
            Assert.assertEquals(contributorsViewModel.hasFailed, true)
        })
    }
}