package com.abelsuviri.viewmodel.mock

object MockJson {
    const val mockRepositoryContributors = "[" +
            "  {" +
            "    \"login\": \"JakeWharton\"," +
            "    \"id\": 66577," +
            "    \"node_id\": \"MDQ6VXNlcjY2NTc3\"," +
            "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/66577?v=4\"," +
            "    \"gravatar_id\": \"\"," +
            "    \"url\": \"https://api.github.com/users/JakeWharton\"," +
            "    \"html_url\": \"https://github.com/JakeWharton\"," +
            "    \"followers_url\": \"https://api.github.com/users/JakeWharton/followers\"," +
            "    \"following_url\": \"https://api.github.com/users/JakeWharton/following{/other_user}\"," +
            "    \"gists_url\": \"https://api.github.com/users/JakeWharton/gists{/gist_id}\"," +
            "    \"starred_url\": \"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}\"," +
            "    \"subscriptions_url\": \"https://api.github.com/users/JakeWharton/subscriptions\"," +
            "    \"organizations_url\": \"https://api.github.com/users/JakeWharton/orgs\"," +
            "    \"repos_url\": \"https://api.github.com/users/JakeWharton/repos\"," +
            "    \"events_url\": \"https://api.github.com/users/JakeWharton/events{/privacy}\"," +
            "    \"received_events_url\": \"https://api.github.com/users/JakeWharton/received_events\"," +
            "    \"type\": \"User\"," +
            "    \"site_admin\": false," +
            "    \"contributions\": 261" +
            "  }," +
            "  {" +
            "    \"login\": \"romainguy\"," +
            "    \"id\": 869684," +
            "    \"node_id\": \"MDQ6VXNlcjg2OTY4NA==\"," +
            "    \"avatar_url\": \"https://avatars1.githubusercontent.com/u/869684?v=4\"," +
            "    \"gravatar_id\": \"\"," +
            "    \"url\": \"https://api.github.com/users/romainguy\"," +
            "    \"html_url\": \"https://github.com/romainguy\"," +
            "    \"followers_url\": \"https://api.github.com/users/romainguy/followers\"," +
            "    \"following_url\": \"https://api.github.com/users/romainguy/following{/other_user}\"," +
            "    \"gists_url\": \"https://api.github.com/users/romainguy/gists{/gist_id}\"," +
            "    \"starred_url\": \"https://api.github.com/users/romainguy/starred{/owner}{/repo}\"," +
            "    \"subscriptions_url\": \"https://api.github.com/users/romainguy/subscriptions\"," +
            "    \"organizations_url\": \"https://api.github.com/users/romainguy/orgs\"," +
            "    \"repos_url\": \"https://api.github.com/users/romainguy/repos\"," +
            "    \"events_url\": \"https://api.github.com/users/romainguy/events{/privacy}\"," +
            "    \"received_events_url\": \"https://api.github.com/users/romainguy/received_events\"," +
            "    \"type\": \"User\"," +
            "    \"site_admin\": false," +
            "    \"contributions\": 64" +
            "  }" +
            "]"

    const val mockUser = "{" +
            "  \"login\": \"JakeWharton\"," +
            "  \"id\": 66577," +
            "  \"node_id\": \"MDQ6VXNlcjY2NTc3\"," +
            "  \"avatar_url\": \"https://avatars0.githubusercontent.com/u/66577?v=4\"," +
            "  \"gravatar_id\": \"\"," +
            "  \"url\": \"https://api.github.com/users/JakeWharton\"," +
            "  \"html_url\": \"https://github.com/JakeWharton\"," +
            "  \"followers_url\": \"https://api.github.com/users/JakeWharton/followers\"," +
            "  \"following_url\": \"https://api.github.com/users/JakeWharton/following{/other_user}\"," +
            "  \"gists_url\": \"https://api.github.com/users/JakeWharton/gists{/gist_id}\"," +
            "  \"starred_url\": \"https://api.github.com/users/JakeWharton/starred{/owner}{/repo}\"," +
            "  \"subscriptions_url\": \"https://api.github.com/users/JakeWharton/subscriptions\"," +
            "  \"organizations_url\": \"https://api.github.com/users/JakeWharton/orgs\"," +
            "  \"repos_url\": \"https://api.github.com/users/JakeWharton/repos\"," +
            "  \"events_url\": \"https://api.github.com/users/JakeWharton/events{/privacy}\"," +
            "  \"received_events_url\": \"https://api.github.com/users/JakeWharton/received_events\"," +
            "  \"type\": \"User\"," +
            "  \"site_admin\": false," +
            "  \"name\": \"Jake Wharton\"," +
            "  \"company\": \"Google, Inc.\"," +
            "  \"blog\": \"http://jakewharton.com\"," +
            "  \"location\": \"Pittsburgh, PA, USA\"," +
            "  \"email\": null," +
            "  \"hireable\": null," +
            "  \"bio\": null," +
            "  \"public_repos\": 101," +
            "  \"public_gists\": 54," +
            "  \"followers\": 52169," +
            "  \"following\": 12," +
            "  \"created_at\": \"2009-03-24T16:09:53Z\"," +
            "  \"updated_at\": \"2019-02-25T21:42:27Z\"" +
            "}"
}