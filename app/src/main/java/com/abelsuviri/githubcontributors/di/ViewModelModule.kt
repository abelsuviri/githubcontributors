package com.abelsuviri.githubcontributors.di

import com.abelsuviri.viewmodel.ContributorsViewModel
import com.abelsuviri.viewmodel.UserLocationViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * @author Abel Suviri
 */

object ViewModelModule {
    val viewModelModule = module {
        viewModel { ContributorsViewModel(get()) }
        viewModel { UserLocationViewModel(get()) }
    }
}