package com.abelsuviri.githubcontributors

import android.app.Application
import com.abelsuviri.githubcontributors.di.NetworkModule.networkModule
import com.abelsuviri.githubcontributors.di.ViewModelModule.viewModelModule
import org.koin.android.ext.android.startKoin

/**
 * @author Abel Suviri
 */

class GitContributorsApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(networkModule, viewModelModule))
    }
}