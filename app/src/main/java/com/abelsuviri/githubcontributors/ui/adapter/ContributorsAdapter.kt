package com.abelsuviri.githubcontributors.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abelsuviri.data.model.ContributorsModel
import com.abelsuviri.githubcontributors.R
import com.abelsuviri.githubcontributors.ui.adapter.viewholder.ContributorsViewHolder
import com.abelsuviri.githubcontributors.ui.adapter.viewholder.ItemClick

/**
 * @author Abel Suviri
 */

class ContributorsAdapter constructor(private val contributorsList: List<ContributorsModel>,
                                      private val itemClick: ItemClick) : RecyclerView.Adapter<ContributorsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContributorsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.contributor_item, parent, false)
        return ContributorsViewHolder(itemView, itemClick)
    }

    override fun onBindViewHolder(holder: ContributorsViewHolder, position: Int) {
        val item = contributorsList[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return contributorsList.size
    }
}