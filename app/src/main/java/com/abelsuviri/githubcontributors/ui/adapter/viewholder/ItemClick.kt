package com.abelsuviri.githubcontributors.ui.adapter.viewholder

/**
 * @author Abel Suviri
 */

interface ItemClick {
    fun onItemClick(username: String)
}