package com.abelsuviri.githubcontributors.ui.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.abelsuviri.data.model.ContributorsModel
import com.abelsuviri.githubcontributors.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.contributor_item.view.*

/**
 * @author Abel Suviri
 */

class ContributorsViewHolder constructor(view: View, private val itemClick: ItemClick) : RecyclerView.ViewHolder(view) {

    fun bind(contributor: ContributorsModel) {
        itemView.usernameTextView.text = contributor.userName
        itemView.commitsTextView.text = itemView.context.getString(R.string.commits, contributor.commits)

        Picasso.get()
            .load(contributor.avatar)
            .into(itemView.avatarImageView)

        itemView.locationButton.setOnClickListener { itemClick.onItemClick(contributor.userName) }
    }
}