package com.abelsuviri.githubcontributors.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abelsuviri.data.model.ContributorsModel
import com.abelsuviri.githubcontributors.R
import com.abelsuviri.githubcontributors.ui.adapter.ContributorsAdapter
import com.abelsuviri.githubcontributors.ui.adapter.viewholder.ItemClick
import com.abelsuviri.viewmodel.ContributorsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @author Abel Suviri
 */

class MainActivity : AppCompatActivity(), ItemClick {

    private val contributorsViewModel: ContributorsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()

        subscribeLiveData()
        contributorsViewModel.getContributors()
    }

    /**
     * Set the RecyclerView properties
     */
    private fun initViews() {
        contributorsRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        contributorsRecyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
    }

    /**
     * Start to observe the LiveDate values from the ViewModel
     */
    private fun subscribeLiveData() {
        contributorsViewModel.isLoading.observe(this, Observer<Boolean> { isLoading -> loadingLayout.visibility = if (isLoading!!) View.VISIBLE else View.GONE })
        contributorsViewModel.hasFailed.observe(this, Observer<Boolean> { hasFailed -> if (hasFailed!!) showRetryDialog() })
        contributorsViewModel.contributorsList.observe(this, Observer<List<ContributorsModel>> { contributorsList ->
            contributorsRecyclerView.adapter = ContributorsAdapter(contributorsList, this)
        })
    }

    /**
     * Callback from a list item click. This will open a new Activity to show the contributor location
     */
    override fun onItemClick(username: String) {
        val intent = Intent(this, UserLocationActivity::class.java)
        intent.putExtra(contributorUsernameExtra, username)
        startActivity(intent)
    }

    /**
     * When there is a response error this retry dialog will be displayed
     */
    private fun showRetryDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(resources.getString(R.string.request_error))
        builder.setCancelable(false)
        builder.setPositiveButton(resources.getString(R.string.retry)) { dialog, i ->
            dialog.dismiss()
        }.show()
    }
}

const val contributorUsernameExtra = "contributorUsernameExtra"
