package com.abelsuviri.githubcontributors.ui

import android.location.Geocoder
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.abelsuviri.githubcontributors.R
import com.abelsuviri.viewmodel.UserLocationViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @author Abel Suviri
 */

class UserLocationActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var userLocation: String
    private lateinit var username: String

    private val userLocationViewModel: UserLocationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_location)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        username = intent.getStringExtra(contributorUsernameExtra)

        subscribeLiveData()
        userLocationViewModel.getContributorLocation(username)
    }

    /**
     * Start to observe the LiveDate values from the ViewModel
     */
    private fun subscribeLiveData() {
        userLocationViewModel.userLocation.observe(this, Observer<String> {
            if (it != null) {
                userLocation = it
                mapFragment.getMapAsync(this)
            } else {
                Toast.makeText(this, "User location not available", Toast.LENGTH_LONG).show()
                finish()
            }
        })
    }

    /**
     * Show the user location in the map
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        val location = getCoordinates()

        map.addMarker(MarkerOptions().position(location).title(username))
        map.moveCamera(CameraUpdateFactory.newLatLng(location))
        map.animateCamera(CameraUpdateFactory.zoomTo(10f))
    }

    /**
     * Get the coordinates for the specified location
     */
    private fun getCoordinates(): LatLng {
        val geocoder = Geocoder(this)
        val address = geocoder.getFromLocationName(userLocation, 1)

        return LatLng(address[0].latitude, address[0].longitude)
    }
}
